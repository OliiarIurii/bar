//
//  BARoomTableViewCell.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

@import UIKit;
#import "BARoom.h"

extern const CGFloat kBARoomTableViewCellHeight;
extern NSString *const kBARoomTableViewCellId;

@interface BARoomTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton *bookBtn;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

- (void)applyData:(BARoom *)room;

@end
