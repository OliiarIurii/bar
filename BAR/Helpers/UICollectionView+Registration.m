//
//  UICollectionView+Registration.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "UICollectionView+Registration.h"

@implementation UICollectionView(Registration)

- (void)registerCellWithReuseId:(NSString *)reuseId
                         useXIB:(BOOL)useXIB {
    if (useXIB) {
        UINib *nib = [UINib nibWithNibName:reuseId
                                    bundle:nil];
        [self registerNib:nib forCellWithReuseIdentifier:reuseId];
        return;
    }
    [self registerClass:NSClassFromString(reuseId) forCellWithReuseIdentifier:reuseId];
}

@end
