//
//  UIImage+Cache.h
//  BAR
//
//  Created by Yurii Oliiar on 7/3/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage(Cache)

+ (void)cachedImage:(NSString *)imageId
       withCallBack:(void (^)(UIImage *image)) completionBlock;

@end
