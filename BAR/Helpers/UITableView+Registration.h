//
//  UITableView+Registration.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface UITableView(Registration)

- (void)registerCellWithReuseId:(NSString *)reuseId
                         useXIB:(BOOL)useXIB;
- (void)registerHeaderFooterWithReuseId:(NSString *)reuseId
                                 useXIB:(BOOL)useXIB;

@end
