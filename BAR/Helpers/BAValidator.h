//
//  BAValidator.h
//  BAR
//
//  Created by Yurii Oliiar on 7/4/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BAValidator : NSObject

+ (BOOL)isValidEmail:(NSString *)email;

@end
