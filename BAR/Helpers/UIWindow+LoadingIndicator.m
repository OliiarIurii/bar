//
//  UIWindow+LoadingIndicator.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "UIWindow+LoadingIndicator.h"
#import "BAColors.h"

static const NSInteger kLoadingIndicatorTag = 0xDEADBEEF;
static const NSInteger kLoadingContainerTag = 0xDEADBEAF;
static const CGFloat kContainerSide = 60.0;

@implementation UIWindow (LoadingIndicator)

- (void)showLoadingIndicator {
    UIView *container = [self viewWithTag:kLoadingContainerTag];
    container.hidden = NO;
    if (container != nil) {
        UIActivityIndicatorView *v = (UIActivityIndicatorView *)[self viewWithTag:kLoadingIndicatorTag];
        [v startAnimating];
        [self bringSubviewToFront:container];
        return;
    }
    UIActivityIndicatorView *loadIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, kContainerSide, kContainerSide)];
    loadIndicator.hidesWhenStopped = YES;
    loadIndicator.tag = kLoadingIndicatorTag;
    loadIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kContainerSide, kContainerSide)];
    container.tag = kLoadingContainerTag;
    container.layer.cornerRadius = 10.0;
    container.center = self.center;
    container.backgroundColor = LIGHT_BLUE_COLOR;
    [container addSubview:loadIndicator];
    
    UIView *coverView = [[UIView alloc] initWithFrame:self.frame];
    coverView.backgroundColor = [UIColor clearColor];
    coverView.tag = kLoadingContainerTag;
    
    [coverView addSubview:container];
    
    [self addSubview:coverView];
    
    
    [loadIndicator startAnimating];
    self.userInteractionEnabled = NO;
    
}

- (void)hideLoadingIndicator {
    UIView *v = [self viewWithTag:kLoadingContainerTag];
    v.hidden = YES;
    [((UIActivityIndicatorView *)[self viewWithTag:kLoadingIndicatorTag]) stopAnimating];
    self.userInteractionEnabled = YES;
}

@end
