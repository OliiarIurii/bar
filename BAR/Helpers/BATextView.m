//
//  BATextView.m
//  BAR
//
//  Created by Yurii Oliiar on 7/3/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BATextView.h"

static const NSInteger kPlaceholderViewTag = 999;

@interface BATextView()

@property (strong, nonatomic) UILabel *placeHolderLabel;

@end

@implementation BATextView

#pragma mark - Init

- (void)insertText:(NSString *)text {
    [super insertText:text];
    [self textChanged];
}
- (void)deleteBackward {
    [super deleteBackward];
    [self textChanged];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    if (!self.placeholder) {
        [self setPlaceholder:@""];
    }
    if (!self.placeholderColor) {
        [self setPlaceholderColor:[UIColor lightGrayColor]];
    }
}

- (id)initWithFrame:(CGRect)frame {
    if( (self = [super initWithFrame:frame])) {
        [self setPlaceholder:@""];
        [self setPlaceholderColor:[UIColor lightGrayColor]];
    }
    return self;
}

#pragma mark - text updates

- (void)textChanged {
    if(_placeholder.length == 0) {
        return;
    }
    [UIView animateWithDuration:0.25
                     animations:^{
                         if(self.text.length == 0) {
                             [[self viewWithTag:kPlaceholderViewTag] setAlpha:1];
                         } else {
                             [[self viewWithTag:kPlaceholderViewTag] setAlpha:0];
                         }
                     }];
}

- (void)drawRect:(CGRect)rect {
    if(self.placeholder.length > 0) {
        if (self.placeHolderLabel == nil ) {
            self.placeHolderLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,
                                                                              0,
                                                                              self.bounds.size.width - 20,
                                                                              self.bounds.size.height)];
            self.placeHolderLabel.lineBreakMode = NSLineBreakByWordWrapping;
            self.placeHolderLabel.numberOfLines = 0;
            self.placeHolderLabel.font = self.font;
            self.placeHolderLabel.backgroundColor = [UIColor clearColor];
            self.placeHolderLabel.textColor = self.placeholderColor;
            self.placeHolderLabel.textAlignment = NSTextAlignmentLeft;
            self.placeHolderLabel.alpha = 0;
            self.placeHolderLabel.tag = kPlaceholderViewTag;
            [self addSubview:self.placeHolderLabel];
        }
        
        self.placeHolderLabel.text = self.placeholder;
        [self sendSubviewToBack:self.placeHolderLabel];
    }
    
    if( self.text.length == 0 && _placeholder.length > 0) {
        [[self viewWithTag:kPlaceholderViewTag] setAlpha:1];
    }
    
    [super drawRect:rect];
}

@end