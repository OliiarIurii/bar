//
//  UITableView+Registration.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "UITableView+Registration.h"

@implementation UITableView(Registration)

- (void)registerCellWithReuseId:(NSString *)reuseId
                         useXIB:(BOOL)useXIB {
    if (useXIB) {
        UINib *nib = [UINib nibWithNibName:reuseId
                                    bundle:nil];
        [self registerNib:nib forCellReuseIdentifier:reuseId];
        return;
    }
    [self registerClass:NSClassFromString(reuseId) forCellReuseIdentifier:reuseId];
}

- (void)registerHeaderFooterWithReuseId:(NSString *)reuseId
                                 useXIB:(BOOL)useXIB {
    if (useXIB) {
        UINib *nib = [UINib nibWithNibName:reuseId
                                    bundle:nil];
        [self registerNib:nib forHeaderFooterViewReuseIdentifier:reuseId];
        return;
    }
    [self registerClass:NSClassFromString(reuseId) forHeaderFooterViewReuseIdentifier:reuseId];
}


@end