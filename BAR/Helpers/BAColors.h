//
//  BAColors.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#ifndef BAColors_h
#define BAColors_h

#define LIGHT_BLUE_COLOR [UIColor colorWithRed:0/255.0 green:169/255.0 blue:219/255.0 alpha:1.0]
#define DARK_BLUE_COLOR [UIColor colorWithRed:54/255.0 green:42/255.0 blue:131/255.0 alpha:1.0]
#define VIOLET_COLOR [UIColor colorWithRed:160/255.0 green:31/255.0 blue:128/255.0 alpha:1.0]
#define PINK_COLOR [UIColor colorWithRed:228/255.0 green:2/255.0 blue:116/255.0 alpha:1.0]
#define RED_COLOR [UIColor colorWithRed:227/255.0 green:30/255.0 blue:47/255.0 alpha:1.0]
#define ORANGE_COLOR [UIColor colorWithRed:236/255.0 green:123/255.0 blue:35/255.0 alpha:1.0]
#define YELLOW_COLOR [UIColor colorWithRed:1 green:233/255.0 blue:0 alpha:1.0]
#define LIGHT_GREEN_COLOR [UIColor colorWithRed:154/255.0 green:189/255.0 blue:54/255.0 alpha:1.0]
#define DARK_GREEN_COLOR [UIColor colorWithRed:0 green:149/255.0 blue:71/255.0 alpha:1.0]

#endif /* BAColors_h */
