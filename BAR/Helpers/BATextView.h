//
//  BATextView.h
//  BAR
//
//  Created by Yurii Oliiar on 7/3/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface BATextView : UITextView

@property (nonatomic, copy) IBInspectable NSString *placeholder;
@property (nonatomic, copy) IBInspectable UIColor *placeholderColor;

@end
