//
//  BAValidator.m
//  BAR
//
//  Created by Yurii Oliiar on 7/4/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAValidator.h"

@implementation BAValidator

+ (BOOL)isValidEmail:(NSString *)email {
    if (email.length == 0) {
        return NO;
    }
    NSString *regex = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    return [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex] evaluateWithObject:email];
}

@end
