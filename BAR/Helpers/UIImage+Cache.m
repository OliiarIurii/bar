//
//  UIImage+Cache.m
//  BAR
//
//  Created by Yurii Oliiar on 7/3/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "UIImage+Cache.h"
#import "BARequest.h"

@implementation UIImage(Cache)


+ (void)cachedImage:(NSString *)imageId
       withCallBack:(void (^)(UIImage *image)) completionBlock {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDir = [NSString stringWithFormat:@"%@/thumbnails",paths.firstObject];
    if (![[NSFileManager defaultManager] fileExistsAtPath:cacheDir]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:cacheDir
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }
    
    NSString *path = [NSString stringWithFormat:@"%@/%@",cacheDir,[imageId lastPathComponent]];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    if (image != nil) {
        completionBlock(image);
        return;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kServerURL,imageId];
    NSURL *url = [NSURL URLWithString:urlString];
    [[[NSURLSession sharedSession] dataTaskWithURL:url
                                completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                    if (data == nil || error != nil) {
                                        completionBlock(nil);
                                    }
                                    UIImage *image = [UIImage imageWithData: data];
                                    completionBlock(image);
                                    
                                    NSFileManager *fm = [NSFileManager defaultManager];
                                    [fm createFileAtPath:path
                                                contents:data
                                              attributes:nil];
                                }] resume];
    
}

@end
