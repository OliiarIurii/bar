//
//  BARoomTableViewCell.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BARoomTableViewCell.h"

#import "UICollectionView+Registration.h"
#import "BATimeCollectionViewCell.h"
#import "BAImageCollectionViewCell.h"

#import "UIImage+Cache.h"

const CGFloat kBARoomTableViewCellHeight = 440;
NSString *const kBARoomTableViewCellId = @"BARoomTableViewCell";

@interface BARoomTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *sizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *capacityLabel;
@property (weak, nonatomic) IBOutlet UILabel *equipmentLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *timeCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *photoCollectionView;

@property (nonatomic, copy) NSArray *availabilities;
@property (nonatomic, copy) NSArray *imageURLs;

@end

@implementation BARoomTableViewCell

- (void)applyData:(BARoom *)room {
    _nameLabel.text = room.name;
    _locationLabel.text = room.location;
    _sizeLabel.text = room.size;
    _capacityLabel.text = [room.capacity description];
    _equipmentLabel.text = [room.equipment componentsJoinedByString:@", "];
    
    self.availabilities = room.avail;
    self.imageURLs = room.images;
    [_timeCollectionView reloadData];
    [_photoCollectionView reloadData];
    
}

- (void)awakeFromNib {
    [_photoCollectionView registerCellWithReuseId:kBAImageCollectionViewCellId
                                           useXIB:YES];
    [_timeCollectionView registerCellWithReuseId:kBATimeCollectionViewCellId
                                           useXIB:YES];
}

#pragma mark - UICollectionView


- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    if (collectionView.tag == 1) {
        return 12;
    }
    
    return _imageURLs.count > 0 ? _imageURLs.count : 1;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView.tag == 1) {
        BATimeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kBATimeCollectionViewCellId
                                                                                   forIndexPath:indexPath];
        cell.titleLabel.text = [NSString stringWithFormat:@"%zi:00",indexPath.row + 7];
        cell.leftView.hidden = YES;
        cell.rightView.hidden = YES;
        
        CGRect hourRect = CGRectMake(indexPath.row, 0, 1, 1);
        for (NSString *availability in _availabilities) {
            NSArray *times = [availability componentsSeparatedByString:@" - "];
            NSArray *comp1 = [[times firstObject] componentsSeparatedByString:@":"];
            NSArray *comp2 = [[times lastObject] componentsSeparatedByString:@":"];
            
            CGFloat x1 = [[comp1 firstObject] intValue] - 7 + [[comp1 lastObject] intValue]/60.0;
            CGFloat x2 = [[comp2 firstObject] intValue] - 7 + [[comp2 lastObject] intValue]/60.0;
            CGRect timeRect = CGRectMake(x1, 0, x2 - x1, 1);
            
            CGRect intersection = CGRectIntersection(hourRect, timeRect);
            if (intersection.size.width == 0) { // no intersection
                continue;
            }
            if (intersection.size.width >= 1.0) { // full instersection
                cell.leftView.hidden = NO;
                cell.leftOffset.constant = 0;
                cell.leftAvailability.constant = kBATimeCollectionViewCellWidth;
                
                return cell;
            }
            CGFloat normalizeX = intersection.origin.x - hourRect.origin.x;
            if (normalizeX < 0.5) {
                cell.leftView.hidden = NO;
                cell.leftAvailability.constant = intersection.size.width * kBATimeCollectionViewCellWidth;
                cell.leftOffset.constant = normalizeX * kBATimeCollectionViewCellWidth;
            } else {
                cell.rightView.hidden = NO;
                cell.rightAvailability.constant = intersection.size.width * kBATimeCollectionViewCellWidth;
                cell.rightOffset.constant = (1 - normalizeX - intersection.size.width) * kBATimeCollectionViewCellWidth;
            }
            
        }
        
        return cell;
    }
    BAImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kBAImageCollectionViewCellId
                                                                                forIndexPath:indexPath];
    if (_imageURLs.count == 0) {
        cell.imageView.image = [UIImage imageNamed:@"imagePlaceholder"];
    } else {
        [UIImage cachedImage:_imageURLs[indexPath.row]
                withCallBack:^(UIImage *image) {
                    cell.imageView.image = image == nil ? [UIImage imageNamed:@"imagePlaceholder"] : image;
                }];
    }
    return cell;
}

@end