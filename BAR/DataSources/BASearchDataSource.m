//
//  BASearchDataSource.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BASearchDataSource.h"

@interface BASearchDataSource ()<UISearchBarDelegate>

@property (nonatomic, assign) id <BASearchDataSourceDelegate> delegate;

@end

@implementation BASearchDataSource

- (instancetype)initWithSearchBar:(UISearchBar *)searchBar
                         delegate:(id<BASearchDataSourceDelegate>)delegate {
    self = [super init];
    if (self) {
        searchBar.delegate = self;
        self.delegate = delegate;
    }
    return self;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    searchBar.text = @"";
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
    [_delegate searchDatasource:self
        didRequestFilterForWord:searchBar.text];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [_delegate searchDatasource:self
        didRequestFilterForWord:searchBar.text];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
    if ([_delegate respondsToSelector:@selector(searchDatasource:didChangeScope:)]) {
        [_delegate searchDatasource:self
                     didChangeScope:selectedScope];
    }
}

@end
