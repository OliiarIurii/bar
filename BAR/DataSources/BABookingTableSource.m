//
//  BABookingTableSource.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BABookingTableSource.h"

#import "UITableView+Registration.h"
#import "BARoomTableViewCell.h"

@interface BABookingTableSource ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) id <BABookingTableSourceDelegate> delegate;
@property (nonatomic, weak) UITableView *tableView;

@end

@implementation BABookingTableSource

- (instancetype)initWithTable:(UITableView *)tableView
                     delegate:(id<BABookingTableSourceDelegate>)delegate {
    self = [super init];
    if (self) {
        self.tableView = tableView;
        [self configureTable];
        self.delegate = delegate;
    }
    return self;
}

- (void)configureTable {
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerCellWithReuseId:kBARoomTableViewCellId
                                 useXIB:YES];
    _tableView.rowHeight = kBARoomTableViewCellHeight;
}

- (void)setRooms:(NSArray<BARoom *> *)rooms {
    _rooms = rooms;
    [_tableView reloadData];
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _rooms.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BARoomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kBARoomTableViewCellId];
    BARoom *r = _rooms[indexPath.row];
    [cell applyData:r];
    cell.separatorView.hidden = _rooms.count == indexPath.row + 1;
    cell.bookBtn.tag = indexPath.row + 1;
    [cell.bookBtn addTarget:self
                     action:@selector(bookRoom:)
           forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)bookRoom:(UIButton *)sender {
    NSInteger index = sender.tag - 1;
    [_delegate roomDatasource:self
               didRequestRoom:_rooms[index]];
}

@end
