//
//  BABookingTableSource.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

@import Foundation;
@import UIKit;
#import "BARoom.h"

@class BABookingTableSource;

@protocol BABookingTableSourceDelegate <NSObject>

- (void)roomDatasource:(BABookingTableSource *)dataSource
        didRequestRoom:(BARoom *)room;

@end

@interface BABookingTableSource : NSObject

@property (nonatomic, copy) NSArray <BARoom *> *rooms;

- (instancetype)initWithTable:(UITableView *)tableView
                     delegate:(id<BABookingTableSourceDelegate>)delegate;

@end
