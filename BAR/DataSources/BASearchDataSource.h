//
//  BASearchDataSource.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

@import Foundation;
@import UIKit;

@class BASearchDataSource;

@protocol BASearchDataSourceDelegate <NSObject>

- (void)searchDatasource:(BASearchDataSource *)dataSource
 didRequestFilterForWord:(NSString *)word;

@optional
- (void)searchDatasource:(BASearchDataSource *)dataSource
          didChangeScope:(NSInteger)selectedScop;

@end


@interface BASearchDataSource : NSObject

- (instancetype)initWithSearchBar:(UISearchBar *)searchBar
                         delegate:(id<BASearchDataSourceDelegate>)delegate;

@end
