//
//  BARoom.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAServerModel.h"

@interface BARoom : BAServerModel

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *location;
@property (nonatomic, copy) NSArray <NSString *> *equipment;
@property (nonatomic, copy) NSString *size; //size of the room in m²
@property (nonatomic, copy) NSNumber *capacity;
@property (nonatomic, copy) NSArray <NSString *> *images;// //can contain 0-3 url to images

// "HH:mm - HH:mm"
// list of times at which a room is free and can be booked,
//between 7 am and 7 pm in 15min steps
@property (nonatomic, copy) NSArray <NSString *> *avail;

- (BOOL)containsWord:(NSString *)word;

@end
