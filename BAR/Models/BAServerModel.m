//
//  BAServerModel.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAServerModel.h"

@implementation BAServerModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    NSAssert(NO, @"initWithDictionary must be overriden");
    return nil;
}

- (NSDictionary *)dictionary {
    NSAssert(NO, @"dictionary must be overriden");
    return nil;
}

@end
