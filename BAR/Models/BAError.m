//
//  BAError.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAError.h"

@implementation BAError

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (dictionary == nil) {
        return nil;
    }
    self = [super init];
    if (self) {
        self.text = dictionary[@"text"];
        self.code = dictionary[@"code"];
    }
    return self;
}

+ (BAError *)errorWithText:(NSString *)text {
    BAError *err = [[BAError alloc] init];
    err.text = text;
    err.code = @0;
    
    return err;
}

@end
