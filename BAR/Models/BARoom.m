//
//  BARoom.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BARoom.h"

@implementation BARoom

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        self.name = dictionary[@"name"];
        self.location = dictionary[@"location"];
        self.equipment = dictionary[@"equipment"];
        self.size = dictionary[@"size"];
        self.capacity = dictionary[@"capacity"];
        self.images = dictionary[@"images"];
        self.avail = dictionary[@"avail"];
    }
    return self;
}

- (BOOL)containsWord:(NSString *)word {
    if ([self.name.lowercaseString containsString:word]) {
        return YES;
    }
    if ([self.location.lowercaseString containsString:word]) {
        return YES;
    }
    for (NSString *equip in _equipment) { // allows partial search inside equipment array
        if ([equip.lowercaseString containsString:word]) {
            return YES;
        }
        
    }
    
    return NO;
}


@end