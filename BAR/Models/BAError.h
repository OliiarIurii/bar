//
//  BAError.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAServerModel.h"

@interface BAError : BAServerModel

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSNumber *code;

+ (BAError *)errorWithText:(NSString *)text;

@end
