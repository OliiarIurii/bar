//
//  BAAttendee.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAAttendee.h"

@implementation BAAttendee

- (NSDictionary *)dictionary {
    return @{@"name" : _name,
             @"email" : _email,
             @"number": _number};
}

@end
