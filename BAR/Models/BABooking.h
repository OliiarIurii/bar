//
//  BABooking.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAServerModel.h"

@interface BABooking : BAServerModel

@property (nonatomic, copy) NSString *date; // UNIX Timestamp/ | "now" | "today",
//only the hour and minute of the time_ timestamps are used,
//the day is always determined by date

@property (nonatomic, copy) NSNumber *time_start; //UNIX Timestamp/,
@property (nonatomic, copy) NSNumber *time_end; //UNIX Timestamp/,
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *descriptionText;
@property (nonatomic, copy) NSString *room; //name of the room that is beeing booked"

@end
