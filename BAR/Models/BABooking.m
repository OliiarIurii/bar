//
//  BABooking.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BABooking.h"

@implementation BABooking

- (NSDictionary *)dictionary {
    return @{@"date" : _date,
             @"time_start" : _time_start,
             @"time_end" : _time_end,
             @"title" : _title,
             @"description" : _descriptionText,
             @"room" : _room};
}

@end
