//
//  BAGetRoomsResponse.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAResponse.h"
#import "BARoom.h"

@interface BAGetRoomsResponse : BAResponse

@property (nonatomic, copy) NSArray <BARoom *> *rooms;

@end
