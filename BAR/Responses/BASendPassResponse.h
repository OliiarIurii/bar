//
//  BASendPassResponse.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAResponse.h"

@interface BASendPassResponse : BAResponse

@property (nonatomic, copy) NSNumber *success;

@end
