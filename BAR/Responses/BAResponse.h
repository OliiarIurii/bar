//
//  BAResponse.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BAError.h"

@interface BAResponse : NSObject

@property (nonatomic, strong) BAError *error;

@end
