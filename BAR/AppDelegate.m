//
//  AppDelegate.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "AppDelegate.h"
#import "BARoomsViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self createUI];
    
    return YES;
}

- (void)createUI {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    BARoomsViewController *roomsVC = [[BARoomsViewController alloc] initWithNibName:@"BARoomsViewController"
                                                                               bundle:nil];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:roomsVC];
    
    self.window.rootViewController = nc;
    [self.window makeKeyAndVisible];
}

@end
