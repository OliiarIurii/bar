//
//  BARequest.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

@import Foundation;
#import "BAResponse.h"

extern NSString *const kServerURL;

@interface BARequest : NSObject

@property (nonatomic, assign) BOOL animationHidden;

- (void)sendWithcompletion:(void (^)(BAResponse *response, NSError *error))completion ;
- (BAResponse *)bindResponseData:(NSData *)responseData;
- (NSString *) methodType;
- (NSString *) methodSignature;
- (NSData *) serialize;

@end
