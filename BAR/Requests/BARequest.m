//
//  BARequest.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BARequest.h"
#import "UIWindow+LoadingIndicator.h"

NSString *const kServerURL = @"https://challenges.1aim.com/roombooking_app/";

@implementation BARequest

- (void)sendWithcompletion:(void (^)(BAResponse *response, NSError *error))completion{
    if (!_animationHidden) {
        [[UIApplication sharedApplication].keyWindow showLoadingIndicator];
    }
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kServerURL,[self methodSignature]];
    NSURL* url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    NSData *requestData = [self serialize];
    request.HTTPBody = requestData;
    request.HTTPMethod = [self methodType];
  
    NSURLSessionDataTask *postDataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                                         completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                             dispatch_async(dispatch_get_main_queue(), ^{
#ifdef DEBUG
                                                                                 NSJSONReadingOptions options =  NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves |NSJSONReadingAllowFragments;
                                                                                 if (data != nil) {
                                                                                     id jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                                    options:options
                                                                                                                                      error:nil];
                                                                                     
                                                                                     NSLog(@"%@",jsonArray);
                                                                                 }
                                                                                 
#endif
                                                                                 if (error != nil) {
                                                                                     NSLog(@"Error %@", [error localizedDescription]);
                                                                                     [[UIApplication sharedApplication].keyWindow hideLoadingIndicator];
                                                                                     completion(nil, error);
                                                                                     return;
                                                                                 }
                                                                                 
                                                                                 NSInteger status = ((NSHTTPURLResponse *)response).statusCode;
                                                                                 if (status != 200 && status != 201) {
                                                                                     NSString *str = [NSString stringWithFormat:@"Status %ti",status];
                                                                                     [[UIApplication sharedApplication].keyWindow hideLoadingIndicator];
                                                                                     completion (nil, [NSError errorWithDomain:@"Response Domain"
                                                                                                                          code:status userInfo:@{NSLocalizedDescriptionKey : str}]);
                                                                                     return;
                                                                                 }
                                                                                 if (error != nil ) {
                                                                                     NSLog(@"Error %@", [error localizedDescription]);
                                                                                     [[UIApplication sharedApplication].keyWindow hideLoadingIndicator];
                                                                                     completion(nil, error);
                                                                                     return;
                                                                                 }
                                                                                 [[UIApplication sharedApplication].keyWindow hideLoadingIndicator];
                                                                                 completion([self bindResponseData:data],nil);
                                                                             });
                                                                         }];
    
    [postDataTask resume];
    
}

- (NSString *)methodSignature {
    NSAssert(NO, @"Should be overriden %@",NSStringFromClass([self class]));
    return @"";
}
- (BAResponse *)bindResponseData:(NSData *)responseData {
    NSAssert(NO, @"Must be overriden");
    return nil;
}

- (NSData *)serialize {
    return nil;
}
- (NSString *) methodType{
    return @"POST";
}


@end
