//
//  BAGetRoomsRequest.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAGetRoomsRequest.h"
#import "BAGetRoomsResponse.h"

NSString *const kDateNow = @"now";
NSString *const kDateToday = @"today";

@implementation BAGetRoomsRequest

- (NSString *)methodSignature {
    return @"getrooms";
}

- (BAResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions
                                                              error:nil];
    
    BAGetRoomsResponse *response = [[BAGetRoomsResponse alloc] init];
    
    if ([jsonDic isKindOfClass:[NSDictionary class]]) {
        BAError *error = [[BAError alloc] initWithDictionary:jsonDic[@"error"]];
        if (error != nil) {
            response.error = error;
            return response;
        }
        response.error = [BAError errorWithText:@"Unknown data format"];
        return response;
    }
    
    NSMutableArray *mutableArr = [NSMutableArray arrayWithCapacity:jsonDic.count];
    for (NSDictionary *dic in jsonDic) {
        BARoom *room = [[BARoom alloc] initWithDictionary:dic];
        if (room != nil) {
            [mutableArr addObject:room];
        }
    }
    response.rooms = mutableArr;
    
    return response;
}

- (NSData *)serialize {
    NSDictionary *dict = @{@"date": _date };
    
    NSError * error = nil;
    NSData *data =  [NSJSONSerialization dataWithJSONObject:dict
                                                    options:NSJSONWritingPrettyPrinted
                                                      error:&error];
    if (error != nil){
        return nil;
    }
    return data;
    
}


@end
