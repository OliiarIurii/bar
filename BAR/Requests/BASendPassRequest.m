//
//  BASendPassRequest.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BASendPassRequest.h"
#import "BASendPassResponse.h"

@implementation BASendPassRequest

- (NSString *)methodSignature {
    return @"sendpass";
}

- (BAResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions
                                                              error:nil];
    
    BASendPassResponse *response = [[BASendPassResponse alloc] init];
    BAError *error = [[BAError alloc] initWithDictionary:jsonDic[@"error"]];
    if (error != nil) {
        response.error = error;
        return response;
    }
    response.success = jsonDic[@"success"];
    
    return response;
}

- (NSData *)serialize {
    NSMutableArray *passes = [[NSMutableArray alloc] initWithCapacity:_passes.count];
    for (BAAttendee *attendee in _passes) {
        [passes addObject:[attendee dictionary]];
    }
    
    
    NSDictionary *dict = @{@"booking" : [_booking dictionary],
                           @"passes" : passes};
    
    NSError * error = nil;
    NSData *data =  [NSJSONSerialization dataWithJSONObject:dict
                                                    options:NSJSONWritingPrettyPrinted
                                                      error:&error];
    if (error != nil){
        return nil;
    }
    return data;
    
}


@end
