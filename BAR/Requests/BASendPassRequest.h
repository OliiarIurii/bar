//
//  BASendPassRequest.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BARequest.h"
#import "BABooking.h"
#import "BAAttendee.h"

@interface BASendPassRequest : BARequest

@property (nonatomic, strong) BABooking *booking;
@property (nonatomic, copy) NSArray<BAAttendee *> *passes;

@end
