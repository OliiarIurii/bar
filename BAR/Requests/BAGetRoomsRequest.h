//
//  BAGetRoomsRequest.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BARequest.h"

extern NSString *const kDateNow;
extern NSString *const kDateToday;

@interface BAGetRoomsRequest : BARequest

@property (nonatomic,copy) NSString *date; ///UNIX Timestamp/ | "now" | "today"

@end
