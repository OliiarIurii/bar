//
//  BABookingViewController.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BABookingViewController.h"
#import "BAAttendeeViewController.h"

#import "UITableView+Registration.h"
#import "BAAttendeeTableViewCell.h"

#import "BATextView.h"
#import "BAAttendee.h"

#import "BASendPassRequest.h"
#import "BASendPassResponse.h"

#import "BAColors.h"

@interface BABookingViewController ()<UITextFieldDelegate, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource, BAAttendeeViewControllerDelegate> {
    BOOL checkingStartDate;
}
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *endButton;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UITextField *titleTextfield;
@property (weak, nonatomic) IBOutlet BATextView *descriptionTextView;

@property (nonatomic, strong) NSMutableArray *attendees;
@property (nonatomic, copy) NSDate *timeStart;
@property (nonatomic, copy) NSDate *timeEnd;
@property (weak, nonatomic) IBOutlet UIView *dateView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateViewBottomOffset;


@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation BABookingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _titleLabel.text = _room.name;
    self.attendees = [NSMutableArray array];
    [self configureTable];
    [self showHideDatePicker];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"HH:mm"];
}

- (void)configureTable {
    [_tableView registerCellWithReuseId:kBAAttendeeTableViewCellId
                                 useXIB:YES];
    _tableView.rowHeight = kBAAttendeeTableViewCellHeight;
}

#pragma mark - IBActions

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)startDatePressed:(id)sender {
    checkingStartDate = YES;
    [self showHideDatePicker];
}

- (IBAction)endDatePressed:(id)sender {
    checkingStartDate = NO;
    [self showHideDatePicker];
}

- (IBAction)addPressed:(id)sender {
    BAAttendeeViewController *vc = [[BAAttendeeViewController alloc] init];
    vc.delegate = self;
    [self.navigationController pushViewController:vc
                                         animated:YES];
}

- (IBAction)donePressed:(id)sender {
    if (_titleLabel.text.length == 0) {
        return;
    }
    if (_descriptionTextView.text.length == 0) {
        return;
    }
    if (_attendees.count == 0) {
        return;
    }
    
    BASendPassRequest *request = [[BASendPassRequest alloc] init];
    request.passes = _attendees;
    
    BABooking *booking = [[BABooking alloc] init];
    booking.title  =_titleTextfield.text;
    booking.descriptionText = _descriptionTextView.text;
    booking.room = _room.name;
    booking.date = _dateString;
    booking.time_start = @([_timeStart timeIntervalSince1970]);
    booking.time_end = @([_timeEnd timeIntervalSince1970]);
    
    request.booking = booking;
    [request sendWithcompletion:^(BAResponse *response, NSError *error) {
        if (error != nil) {
            [self showMessage:error.localizedDescription];
            return;
        }
        if (response.error != nil) {
            [self showMessage:response.error.text];
            return;
        }
        [self showMessage:@"Room Booked"];
    }];
}

#pragma mark - time picker

- (void)showHideDatePicker {
    [self.view bringSubviewToFront:_dateView];
    CGFloat constant = _dateViewBottomOffset.constant < 0 ? 0 : -_dateView.frame.size.height;
    [UIView animateWithDuration:0.4
                     animations:^{
                         _dateViewBottomOffset.constant = constant;
                         [self.view layoutIfNeeded];
                     }];
}

- (IBAction)cancelPressed:(id)sender {
    [self showHideDatePicker];
}

- (IBAction)doneDatePressed:(id)sender {
    [self showHideDatePicker];
    if (checkingStartDate) {
        self.timeStart = _datePicker.date;
        NSString *title = [NSString stringWithFormat:@"Start : %@",[_dateFormatter stringFromDate:_timeStart]];
        [_startButton setTitle:title
                      forState:UIControlStateNormal];
        return;
    }
    self.timeEnd = _datePicker.date;
    NSString *title = [NSString stringWithFormat:@"End : %@",[_dateFormatter stringFromDate:_timeEnd]];
    [_endButton setTitle:title
                forState:UIControlStateNormal];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [_descriptionTextView resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _attendees.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BAAttendeeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kBAAttendeeTableViewCellId];
    BAAttendee *attendee = _attendees[indexPath.row];
    
    cell.nameLabel.text = attendee.name;
    cell.emailLabel.text = attendee.email;
    cell.phoneLabel.text = [attendee.number description];
    cell.separatorView.hidden = indexPath.row  == _attendees.count - 1;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView
                  editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewRowAction *remove = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault
                                                                      title:@"DELETE"
                                                                    handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
                                                                        [_attendees removeObjectAtIndex:indexPath.row];
                                                                        [_tableView reloadData];
                                                                    }];
    remove.backgroundColor = RED_COLOR;
    
    
    UITableViewRowAction *edit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault
                                                                    title:@"Edit"
                                                                  handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
                                                                      BAAttendeeViewController *vc = [[BAAttendeeViewController alloc] init];
                                                                      vc.attendee  =_attendees[indexPath.row];
                                                                      vc.delegate = self;
                                                                      [self.navigationController pushViewController:vc
                                                                                                           animated:YES];
                                                                  }];
    edit.backgroundColor = VIOLET_COLOR;
    
    return @[remove,edit];
}

#pragma mark - BAAttendeeViewControllerDelegate

- (void)attendeeVC:(BAAttendeeViewController *)vc didFinishWithAttendee:(BAAttendee *)attendee {
    if (vc.attendee  != nil) {
        [_attendees replaceObjectAtIndex:[_attendees indexOfObject:vc.attendee]
                              withObject:attendee];
    } else {
        [_attendees addObject:attendee];
    }
    [_tableView reloadData];
    [self.navigationController popViewControllerAnimated:YES];
}

@end