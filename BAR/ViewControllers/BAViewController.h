//
//  BAViewController.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^BAVoidCompletionBlock)(void);

@interface BAViewController : UIViewController

- (void)showMessage:(NSString *)message;
- (void)showMessage:(NSString *)message
         completion:(BAVoidCompletionBlock) completionBlock;

@end
