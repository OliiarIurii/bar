//
//  BAViewController.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAViewController.h"

@interface BAViewController ()

@end

@implementation BAViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
}

- (void)showMessage:(NSString *)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"BAR"
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok"
                                              style:UIAlertActionStyleDefault
                                            handler:NULL]];
    
    [self presentViewController:alert
                       animated:YES
                     completion:NULL];
}

- (void)showMessage:(NSString *)message
         completion:(BAVoidCompletionBlock) completionBlock {
   
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"BAR"
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * _Nonnull action) {
                                                completionBlock();
                                            }]];
    
    [self presentViewController:alert
                       animated:YES
                     completion:NULL];
    
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
