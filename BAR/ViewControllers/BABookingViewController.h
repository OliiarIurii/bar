//
//  BABookingViewController.h
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAViewController.h"
#import "BARoom.h"

@interface BABookingViewController : BAViewController

@property (nonatomic, strong) BARoom *room;
@property (nonatomic, copy) NSString *dateString;

@end
