//
//  BARoomsViewController.m
//  BAR
//
//  Created by Yurii Oliiar on 7/2/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BARoomsViewController.h"
#import "BABookingViewController.h"

#import "BASearchDataSource.h"
#import "BABookingTableSource.h"

#import "BAGetRoomsRequest.h"
#import "BAGetRoomsResponse.h"

typedef NS_ENUM(NSInteger, BAViewType) {
    BAViewTypeAll,
    BAViewTypeAvailable
};

@interface BARoomsViewController ()<BASearchDataSourceDelegate,BABookingTableSourceDelegate>

@property (nonatomic, assign) BAViewType viewType;
@property (nonatomic, strong) BASearchDataSource *searchDataSource;
@property (nonatomic, strong) BABookingTableSource *tableDataSource;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateViewBottomOffset;

@property (nonatomic, copy) NSArray <BARoom *> *allRooms;
@property (weak, nonatomic) IBOutlet UIView *dateView;

@property (nonatomic, copy) NSString *dateString;
@property (nonatomic, copy) NSDate *date;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (weak, nonatomic) IBOutlet UIButton *dateTitleButton;

@end

@implementation BARoomsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureDate];
    [self configureUI];
    [self loadData];
    [self showHideDatePicker];
}

- (void)loadData {
    BAGetRoomsRequest *request = [[BAGetRoomsRequest alloc] init];
    request.date = _dateString;
    [request sendWithcompletion:^(BAResponse *response, NSError *error) {
        if (error != nil) {
            [self showMessage:error.localizedDescription];
            return;
        }
        if (response.error != nil) {
            [self showMessage:response.error.text];
            return;
        }
        BAGetRoomsResponse *r = (BAGetRoomsResponse *)response;
        self.allRooms = r.rooms;
        [self filterDataForWord:_searchBar.text];
    }];
}

- (void)configureUI {
    self.searchDataSource = [[BASearchDataSource alloc] initWithSearchBar:_searchBar
                                                                 delegate:self];
    self.tableDataSource = [[BABookingTableSource alloc] initWithTable:_tableView
                                                              delegate:self];
}

- (void)configureDate {
    self.dateString = kDateToday;
    self.date = [NSDate date];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"dd.MM.yyyy"];
}

#pragma mark - Delegate methods

- (void)searchDatasource:(BASearchDataSource *)dataSource didRequestFilterForWord:(NSString *)word {
    [self filterDataForWord:word];
}

- (void)searchDatasource:(BASearchDataSource *)dataSource didChangeScope:(NSInteger)selectedScope {
    self.viewType = selectedScope;
    [self filterDataForWord:_searchBar.text];
}

- (void)roomDatasource:(BABookingTableSource *)dataSource didRequestRoom:(BARoom *)room {
    BABookingViewController *vc = [[BABookingViewController alloc] init];
    vc.room = room;
    vc.dateString = _dateString;
    [self.navigationController pushViewController:vc
                                         animated:YES];
}

#pragma mark - Helpers

- (void)filterDataForWord:(NSString *)word {
    if (word.length == 0) {
        _tableDataSource.rooms = [self roomsFilteredByType:_allRooms];
        return;
    }
    NSString *searchWord = word.lowercaseString;
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (BARoom *room in _tableDataSource.rooms) {
        if ([room containsWord:searchWord]) {
            [arr addObject:room];
        }
    }
    _tableDataSource.rooms = [self roomsFilteredByType:arr];
}

- (NSArray<BARoom *> *)roomsFilteredByType:(NSArray<BARoom *> *)array {
    if (_viewType == BAViewTypeAll) {
        return array;
    }
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (BARoom *room in array) {
        if (room.avail.count > 0) {
            [result addObject:room];
        }
    }
    return result;
}

#pragma mark - Helpers

- (void)showHideDatePicker {
    [self.view bringSubviewToFront:_dateView];
    CGFloat constant = _dateViewBottomOffset.constant < 0 ? 0 : -_dateView.frame.size.height;
    [UIView animateWithDuration:0.4
                     animations:^{
                         _dateViewBottomOffset.constant = constant;
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - IBActions

- (IBAction)prevDayPressed:(id)sender {
    NSDate *yesterday = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                                 value:-1
                                                                toDate:_date
                                                               options:0];
    
    _datePicker.date = yesterday;
    self.date = yesterday;
    [_dateTitleButton setTitle:[_dateFormatter stringFromDate:_date]
                      forState:UIControlStateNormal];
    self.dateString = @([_date timeIntervalSince1970]).description;
    [self loadData];
}

- (IBAction)nextDayPressed:(id)sender {
    NSDate *tommorrow = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                                 value:1
                                                                toDate:_date
                                                               options:0];
    
    _datePicker.date = tommorrow;
    self.date = tommorrow;
    [_dateTitleButton setTitle:[_dateFormatter stringFromDate:_date]
                      forState:UIControlStateNormal];
    self.dateString = @([_date timeIntervalSince1970]).description;
    [self loadData];
}

- (IBAction)dateBtnPressed:(id)sender {
    [self showHideDatePicker];
}

- (IBAction)cancelPressed:(id)sender {
    [self showHideDatePicker];
}

- (IBAction)donePressed:(id)sender {
    [self showHideDatePicker];
    self.date = _datePicker.date;
    [_dateTitleButton setTitle:[_dateFormatter stringFromDate:_date]
                      forState:UIControlStateNormal];
    self.dateString = @([_date timeIntervalSince1970]).description;
    [self loadData];
}

- (IBAction)todayPressed:(id)sender {
    [self showHideDatePicker];
    self.date = [NSDate date];;
    [_dateTitleButton setTitle:@"TODAY"
                      forState:UIControlStateNormal];
    self.dateString = kDateToday;
    [self loadData];
}

- (IBAction)nowPressed:(id)sender {
    [self showHideDatePicker];
    self.date = [NSDate date];;
    [_dateTitleButton setTitle:@"NOW"
                      forState:UIControlStateNormal];
    self.dateString = kDateNow;
    [self loadData];
}

@end