//
//  BAAttendeeViewController.h
//  BAR
//
//  Created by Yurii Oliiar on 7/4/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAViewController.h"
#import "BAAttendee.h"

@class BAAttendeeViewController;

@protocol BAAttendeeViewControllerDelegate <NSObject>

- (void)attendeeVC:(BAAttendeeViewController *)vc didFinishWithAttendee:(BAAttendee *)attendee;

@end

@interface BAAttendeeViewController : BAViewController

@property (nonatomic, assign) id<BAAttendeeViewControllerDelegate> delegate;
@property (nonatomic, strong) BAAttendee *attendee;

@end
