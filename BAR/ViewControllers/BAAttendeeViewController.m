//
//  BAAttendeeViewController.m
//  BAR
//
//  Created by Yurii Oliiar on 7/4/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAAttendeeViewController.h"
#import "BAValidator.h"
#import "BAColors.h"

@interface BAAttendeeViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;

@end

@implementation BAAttendeeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _nameTextField.text = _attendee.name;
    _emailTextField.text = _attendee.email;
    _phoneTextField.text = [_attendee.number description];
    
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:self
                                                                          action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"DONE"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(endNumberInput)];
    doneButton.tintColor = LIGHT_BLUE_COLOR;
    [keyboardDoneButtonView setItems:@[flex,doneButton]];
    _phoneTextField.inputAccessoryView = keyboardDoneButtonView;
}

- (void)endNumberInput {
    [_phoneTextField resignFirstResponder];
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)donePressed:(id)sender {
    if (![BAValidator isValidEmail:_emailTextField.text]) {
        [self showMessage:@"Please, provide valid email address"];
        return;
    }
    
    NSCharacterSet *whitespaceAndNewlineCharacterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *name = [_nameTextField.text stringByTrimmingCharactersInSet:whitespaceAndNewlineCharacterSet];
    if (name.length == 0) {
        return;
    }
    
    if (_phoneTextField.text.length < 5) {
        [self showMessage:@"Please, provide valid phone number"];
        return;
    }
    BAAttendee *a = [[BAAttendee alloc] init];
    a.name = _nameTextField.text;
    a.number = @(_phoneTextField.text.integerValue);
    a.email = _emailTextField.text;
    [_delegate attendeeVC:self didFinishWithAttendee:a];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}

@end
