//
//  BATimeCollectionViewCell.h
//  BAR
//
//  Created by Yurii Oliiar on 7/3/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const CGFloat kBATimeCollectionViewCellWidth;
extern NSString *const kBATimeCollectionViewCellId;

@interface BATimeCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftAvailability;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightAvailability;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftOffset;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightOffset;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIView *rightView;

@end