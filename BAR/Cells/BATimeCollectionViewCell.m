//
//  BATimeCollectionViewCell.m
//  BAR
//
//  Created by Yurii Oliiar on 7/3/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BATimeCollectionViewCell.h"

const CGFloat kBATimeCollectionViewCellWidth = 50;
NSString *const kBATimeCollectionViewCellId = @"BATimeCollectionViewCell";

@implementation BATimeCollectionViewCell
@end
