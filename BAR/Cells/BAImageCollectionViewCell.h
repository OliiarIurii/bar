//
//  BAImageCollectionViewCell.h
//  BAR
//
//  Created by Yurii Oliiar on 7/3/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const kBAImageCollectionViewCellId;

@interface BAImageCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;


@end
