//
//  BAAttendeeTableViewCell.m
//  BAR
//
//  Created by Yurii Oliiar on 7/3/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAAttendeeTableViewCell.h"

NSString *const kBAAttendeeTableViewCellId = @"BAAttendeeTableViewCell";
const CGFloat kBAAttendeeTableViewCellHeight = 80;

@implementation BAAttendeeTableViewCell
@end