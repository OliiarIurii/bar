//
//  BAImageCollectionViewCell.m
//  BAR
//
//  Created by Yurii Oliiar on 7/3/16.
//  Copyright © 2016 Iurii Oliiar. All rights reserved.
//

#import "BAImageCollectionViewCell.h"

NSString *const kBAImageCollectionViewCellId = @"BAImageCollectionViewCell";

@implementation BAImageCollectionViewCell
@end
